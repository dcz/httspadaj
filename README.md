# HTTP/HTTPS ddos-protecting proxy

This is a fast proxy which doesn't allow any connections without a cookie pass through to the underlying server. Any DDoS attacker must now keep the cookie state to get to the protected application.

It's written in actix-web, so it's supposed to be super-fast.

It expects the certificate and key file to be provided as environment variables.

To start:

```shell
cargo run -- <listen addr> <listen port> <tls port> <forward host> <forward port>
# example:
SPADAJ_KEY="key.pem" SPADAJ_CERT="cert.pem" cargo run -- 127.0.0.1 8000 4433 127.0.0.1 9900
```

To build for production:

```
cargo build --release
```

The executable file will be found in target/release/httspadaj.

## Deploying

Only the httspadaj file needs to be copied.

This proxy should go between the open Web and the existing server. Therefore, the existing server **must** be reconfigured!

1. Remove HTTP to HTTPS redirect.
2. Change HTTP port to a localhost port.
3. Stop serving HTTPS on 443. If it must stay open, bind it on localhost.
4. Configure httspadaj to use the same TLS certificates as previous config.
5. Run `./httspadaj 0.0.0.0 80 443 localhost $HTTP_PORT`