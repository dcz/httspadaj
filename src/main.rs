// SPDX-License-Identifier: Apache-2.0

use std::net::ToSocketAddrs;

use actix_web::{
    cookie::Cookie, dev::PeerAddr, error, http::header, middleware, web, App, Error, HttpRequest, HttpResponse,
    HttpServer,
};
use awc::Client;
use clap::Parser;
use rustls;
use rustls::{Certificate, PrivateKey};
use rustls_pemfile;
use rustls_pemfile::pkcs8_private_keys;
use std::env;
use std::fs::File;
use std::io::BufReader;
use std::net::SocketAddr;


const COOKIE: &'static str = "spadaj";


/// Forwards the incoming HTTP request using `awc`.
async fn forward(
    req: HttpRequest,
    payload: web::Payload,
    peer_addr: Option<PeerAddr>,
    addr: web::Data<SocketAddr>,
    client: web::Data<Client>,
) -> Result<HttpResponse, Error> {
    let query = req.uri()
        .path_and_query()
        .ok_or(error::ErrorInternalServerError("Can't forward when there's no query"))?
        .as_str();
    
    // TODO: Derive the hash value from the IP address and a secret.
    if req.cookie(COOKIE).map(|c| c.value() == "hash") != Some(true) {
        return Ok(
            HttpResponse::TemporaryRedirect()
                .append_header((
                    "Location",
                    String::from(query),
                ))
                .cookie(Cookie::new(COOKIE, "hash"))
                .finish()
        );
    }
    
    let host = match req.uri().host() {
        Some(h) => h,
        None => req.headers()
            .get(header::HOST)
            .ok_or(error::ErrorInternalServerError("Can't forward when there's no host"))
            .and_then(|h|
                h.to_str().map_err(error::ErrorInternalServerError)
            )?,
    };


    // I'm not sure if it's possible to communicate the other host if TLS was used. Maybe x-tls?
    // The standard headers specify "Host", but no "Tls".
    // The request library needs the protocol to know how to make the connection.
    let mut uri = String::from("http://");
    uri.push_str(host);
    uri.push_str(query);
    let uri = uri;
    
    let forwarded_req = client
        .request_from(uri, req.head())
        .address(**addr)
        .no_decompress();

    // TODO: This forwarded implementation is incomplete as it only handles the unofficial
    // X-Forwarded-For header but not the official Forwarded one.
    let forwarded_req = match peer_addr {
        Some(PeerAddr(addr)) => {
            forwarded_req.insert_header(("x-forwarded-for", addr.ip().to_string()))
        }
        None => forwarded_req,
    };

    let res = forwarded_req
        .send_stream(payload)
        .await
        .map_err(error::ErrorInternalServerError)?;

    let mut client_resp = HttpResponse::build(res.status());
    // Remove `Connection` as per
    // https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Connection#Directives
    for (header_name, header_value) in res.headers().iter().filter(|(h, _)| *h != "connection") {
        client_resp.insert_header((header_name.clone(), header_value.clone()));
    }

    Ok(client_resp.streaming(res))
}

fn load_rustls_config() -> rustls::ServerConfig {
    // init server config builder with safe defaults
    let config = rustls::ServerConfig::builder()
        .with_safe_defaults()
        .with_no_client_auth();

    // load TLS key/cert files
    let cert_file = &mut BufReader::new(File::open(
        env::var("SPADAJ_CERT").expect("SPADAJ_CERT env var missing")
    ).unwrap());
    let key_file = &mut BufReader::new(File::open(
        env::var("SPADAJ_KEY").expect("SPADAJ_KEY env var missing")
    ).unwrap());

    // convert files to key/cert objects
    let cert_chain = rustls_pemfile::certs(cert_file)
        .unwrap()
        .into_iter()
        .map(Certificate)
        .collect();
    let mut keys: Vec<PrivateKey> = pkcs8_private_keys(key_file)
        .unwrap()
        .into_iter()
        .map(PrivateKey)
        .collect();

    // exit if no keys could be parsed
    if keys.is_empty() {
        eprintln!("Could not locate PKCS 8 private keys.");
        std::process::exit(1);
    }

    config.with_single_cert(cert_chain, keys.remove(0)).unwrap()
}

#[derive(clap::Parser, Debug)]
struct CliArguments {
    listen_addr: String,
    listen_port: u16,
    tls_port: u16,
    forward_addr: String,
    forward_port: u16,
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));

    let args = CliArguments::parse();

    let forward_socket_addr: SocketAddr = (args.forward_addr, args.forward_port)
        .to_socket_addrs()?
        .next()
        .expect("given forwarding address was not valid");

    let config = load_rustls_config();
    
    log::info!(
        "starting HTTP server at http://{}:{}",
        &args.listen_addr,
        args.listen_port
    );

    log::info!("forwarding to {forward_socket_addr}");

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(Client::default()))
            .app_data(web::Data::new(forward_socket_addr))
            .wrap(middleware::Logger::default())
            .default_service(web::to(forward))
    })
    .bind((args.listen_addr.clone(), args.listen_port))?
    .bind_rustls((args.listen_addr, args.tls_port), config
    )?
    .run()
    .await
}
